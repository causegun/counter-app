# Counter App

## Task Description

Let's imagine that you need to implement a counter app. However, the app should manage the counter separately so it can be accessed from any screen in the app. To achieve it, we will use a bound service. So, any new activity can bind to the service and start using the counter. The service should allow you to present the current counter value (using a toast notification), increment, and decrement it.

## Complete the Task

- Create a program that meets the requirements from the description above. You need to take the following steps:

- Create an activity with a text view that will display a counter and three buttons: increment, decrement, and present value.
- Create a bound service to manage the counter with three methods to present the current counter value (using a toast in the service), increment, and decrement it.
