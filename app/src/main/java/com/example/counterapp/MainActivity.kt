package com.example.counterapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import com.example.counterapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var valueEditService: ValueEditService
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as ValueEditService.ValueEditServiceBinder
            valueEditService = binder.getService()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            incrementButton.setOnClickListener { valueEditService.incrementValue() }
            decrementButton.setOnClickListener { valueEditService.decrementValue()}
            presentValueButton.setOnClickListener { valueEditService.showValue() }
        }
    }

    override fun onStart() {
        super.onStart()
        Intent(this, ValueEditService::class.java).also {
            bindService(it, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
    }
}