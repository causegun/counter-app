package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class ValueEditService : Service() {

    private val binder = ValueEditServiceBinder()
    private var value = 0

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    inner class ValueEditServiceBinder : Binder() {
        fun getService() = this@ValueEditService
    }

    fun incrementValue() {
        value++
    }

    fun decrementValue() {
        value--
    }

    fun getValue(): Int {
        return value
    }

    fun showValue() {
        Toast.makeText(this, getValue().toString(), Toast.LENGTH_LONG).show()
    }
}